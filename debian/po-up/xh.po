# Xhosa translation of gdm2
# Copyright (C) 2005 Canonical Ltd.
# This file is distributed under the same license as the gdm2 package.
# Translation by Canonical Ltd <translations@canonical.com> with thanks to
# Translation World CC in South Africa, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: gdm2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-28 22:48+0200\n"
"PO-Revision-Date: 2005-03-03 12:39+0200\n"
"Last-Translator: Canonical Ltd <translations@canonical.com>\n"
"Language-Team: Xhosa <xh-translate@ubuntu.com> \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../default.desktop.in.h:1
msgid "System Default"
msgstr "Inkqubo Emiselweyo"

#: ../default.desktop.in.h:2
msgid "The default session on the system"
msgstr ""

#: ../patches/11_xephyr_nested.patch:747
msgid "Run gdm in a nested window"
msgstr ""

#: ../patches/11_xephyr_nested.patch:748
msgid "Don't lock the screen on the current display"
msgstr ""

#: ../patches/11_xephyr_nested.patch:1052
msgid "Disconnect"
msgstr "Khulula"

#: ../patches/11_xephyr_nested.patch:1056
msgid "Quit"
msgstr "Phuma"
