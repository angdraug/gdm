# Persian translation of gdm2.
# Copyright (C) 2003, 2004, 2005, 2006 Sharif FarsiWeb, Inc.
# This file is distributed under the same license as the gdm2 package.
# Roozbeh Pournader <roozbeh@farsiweb.info>, 2003, 2005.
# Meelad Zakaria <meelad@farsiweb.info>, 2004, 2005, 2006.
# Alireza Kheirkhahan <kheirkhahan@gmail.com>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: gdm2 2.6.0.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-28 22:48+0200\n"
"PO-Revision-Date: 2006-03-13 17:57+0330\n"
"Last-Translator: Meelad Zakaria <meelad@farsiweb.info>\n"
"Language-Team: Persian \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../default.desktop.in.h:1
msgid "System Default"
msgstr "پیش‌فرض سیستم"

#: ../default.desktop.in.h:2
msgid "The default session on the system"
msgstr ""

#: ../patches/11_xephyr_nested.patch:747
msgid "Run gdm in a nested window"
msgstr ""

#: ../patches/11_xephyr_nested.patch:748
msgid "Don't lock the screen on the current display"
msgstr ""

#: ../patches/11_xephyr_nested.patch:1052
msgid "Disconnect"
msgstr "قطع اتصال"

#: ../patches/11_xephyr_nested.patch:1056
msgid "Quit"
msgstr "ترک"
